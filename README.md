# ProjetDevWeb (Docker)

Ce repository contient le fichier docker-compose.yml pour lancer l'ensemble des composants de l'application

## Exécution 

Pour exécuter l'application, utiliser la commande `docker-compose up -d`.
L'application frontend sera disponible sur le port 8080 et l'application backend sur le port 8081 ([lien swagger](http://localhost:8081/swagger-ui/index.html)).

## DockerHub

- [Frontend](https://hub.docker.com/r/ossacipe/projetdevweb-frontend)
- [Backend](https://hub.docker.com/r/ossacipe/projetdevweb-backend)

## Code source

- [Frontend](https://gitlab.com/p7115/frontend)
- [Backend](https://gitlab.com/p7115/backend)

